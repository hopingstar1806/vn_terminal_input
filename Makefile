CC=clang
CFLAGS=-Wall -std=c17

OBJ=    array_clear.o \
	command.o \
	input_method.o \
	vn_terminal_input.o

PROG=   vn_terminal_input

all:
	$(CC) $(CFLAGS) -c array_clear.c
	$(CC) $(CFLAGS) -c command.c
	$(CC) $(CFLAGS) -c input_method.c
	$(CC) $(CFLAGS) -c vn_terminal_input.c	
	$(CC) -o $(PROG) $(OBJ)

clean:
	rm -rf $(PROG) *.o
