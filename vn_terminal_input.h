#define MAX     100

typedef enum { TELEX = 0, VNI } Mode;

void command(const wchar_t *, Mode *);
void clear_one_element(wchar_t *, unsigned int);
void clear_two_element(wchar_t *, unsigned int);
void clear_words(wchar_t *);
void telex_method(wchar_t *, unsigned int i);
void vni_method(wchar_t *, unsigned int i);
