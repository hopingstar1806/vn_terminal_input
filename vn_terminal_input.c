#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include "vn_terminal_input.h"

int
main(void)
{
	wchar_t         words[MAX];
	Mode            input_mode = TELEX;

	setlocale(LC_CTYPE, "en_US.UTF-8");
	system("clear");
	printf("By default input mode will be set to Telex.\n");
	printf("Type /help to print a list of usage command.\n\n");

	for(;;){
		/* Cleaning input */
		clear_words(words);

		/* Get user input */
		printf("Enter your message:\n");
		fgetws(words, MAX, stdin);

		/* Check for command */
		if (words[0] == '/'){
			command(words, &input_mode);
			continue;
		}

		for (unsigned int i = 0U; ; ++i){
			if(input_mode == TELEX)
				telex_method(words, i);
			else if(input_mode == VNI)
				vni_method(words, i);

			if(words[i] == '\0')
				break;
		}

		printf("%ls\n", words);
	}
}
