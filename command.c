#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>

#include "vn_terminal_input.h"

void
command(const wchar_t *words, Mode *input_mode)
{
	/* Clear screen command */
	if (wcsncmp(words, L"/clear", 6) == 0){
		system("clear");
	}

	/* Quit program command */
	if (wcsncmp(words, L"/exit", 5) == 0){
		exit(EXIT_SUCCESS);
	}

	/* Print list of command */
	if (wcsncmp(words, L"/help", 5) == 0){
		system("clear");
		printf("Usage command\n\n");
		printf("/clear:\t\tclear sceen.\n");
		printf("/exit:\t\texit program.\n");
		printf("/help:\t\tprint list of usage command.\n");
		printf("/mode show:\tshow your current input mode.\n");
		printf("/mode telex:\tswitch to Telex mode.\n");
		printf("/mode vni:\tswitch to Vni mode.\n");
	}

	/* Print input mode command */
	if (wcsncmp(words, L"/mode show", 10) == 0){
		if (*input_mode == TELEX)
			printf("Current mode is Telex.\n");
		else if (*input_mode == VNI)
			printf("Current mode is Vni.\n");
	}

	/* Switch to Telex mode command */
	if (wcsncmp(words, L"/mode telex", 11) == 0){
		*input_mode = TELEX;
		printf("Switch successfully to telex.\n");
	}

	/* Switch to VNI mode command */
	if (wcsncmp(words, L"/mode vni", 9) == 0){
		*input_mode = VNI;
		printf("Switch successfully to vni.\n");
	}
}
