# vn_terminal_input

A student project.
Vietnamese terminal input written in C standard.

To build:
```
make
```

To run:
```
./vn_terminal_input
```

For more information how to use software after
you run it please type:
```
/help
```

To clean:
```
make clean
```
